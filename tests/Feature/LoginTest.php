<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Caffeinated\Shinobi\Models\Role;

class LoginTest extends TestCase
{
    /** @var \App\User */
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->user->assignRole(
            factory(Role::class)->create(['special' => 'all-access'])->id
        );
    }

    /** @test */
    function authenticate()
    {
        $this->postJson('/api/login', [
            'email' => $this->user->email,
            'password' => 'secret',
        ])
        ->assertSuccessful()
        ->assertJsonStructure(['token', 'expires_in'])
        ->assertJson(['token_type' => 'bearer']);
    }

    /** @test */
    function fetch_the_current_user()
    {
        $this->actingAs($this->user)
            ->getJson('/api/auth')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email', 'role'
                ]
            ]);
    }

    /** @test */
    function log_out()
    {
        $token = $this->postJson('/api/login', [
            'email' => $this->user->email,
            'password' => 'secret',
        ])->json()['token'];

        $this->postJson("/api/logout?token=$token")
            ->assertSuccessful();

        $this->getJson("/api/auth?token=$token")
            ->assertStatus(401);
    }
}
