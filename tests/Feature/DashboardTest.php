<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\User;
use App\Parking;
use App\Vehicle;
use Carbon\Carbon;
use Faker\Factory as Faker;

class DashboardTest extends TestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function get_parking_info()
    {
        $this->actingAs($this->user)
            ->getJson('/api/parking/data')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        '*' => [
                            'id', 'tag', 'status'
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function get_tickets_list()
    {
        $this->setUpParking();
        $this->setUpParkingCell(5);
        $tickets = Parking::getTickects();

        $this->actingAs($this->user)
            ->getJson('/api/parking/tickets')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'ticket', 'tag', 'vehicle', 'logs', 'status', 'set_at'
                    ]
                ]
            ])
            ->assertJsonFragment($tickets->random()->toArray());
    }

    /** @test */
    public function get_busiest_cell_parking()
    {
        $this->setUpParking();
        $this->setUpParkingCell(20, true);
        $cells = Parking::getBusiestCell();

        $this->actingAs($this->user)
            ->getJson('/api/parking/busiest')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'tag', 'time', 'total'
                    ]
                ]
            ])
            ->assertJsonFragment($cells->random()->toArray());

    }

    /** @test */
    public function vehicle_in_parking()
    {
        $faker = Faker::create();
        $this->setUpParking();
        $this->setUpParkingCell(3, true);
        $data = [
            'brand' => strtoupper($faker->randomElement([
                'Chevrolet', 'Fiat', 'Ford', 'Jeep', 'Toyota'
            ])), 
            'license_plate' => strtoupper(strtoupper(str_random(6)))
        ];

        $this->actingAs($this->user)
            ->json('POST', '/api/parking/in/vehicle', $data)
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'ticket' => [
                        'id', 'tag', 'status', 'vehicle', 'logs'
                    ]
                ]
            ]);
        $this->assertDatabaseHas('vehicles', $data);
        $vehicle = Vehicle::where('brand', $data['brand'])->first();
        $this->assertDatabaseHas('parkings', [
            'vehicle_id' => $vehicle->id
        ]);
    }

    /** @test */
    public function get_data_license_plate_ticket()
    {
        $this->setUpParking();
        $this->setUpParkingCell(2);
        $vehicle = Vehicle::first();
        $data = [ 'key' => $vehicle->license_plate];

        $this->actingAs($this->user)
            ->json('GET', '/api/parking/vehicle', $data)
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id', 'tag', 'status', 'vehicle', 'logs', 'time', 'total'
                ]
            ]);
        
    }

    /** @test */
    public function vehicle_out_parking()
    {
        $this->setUpParking();
        $this->setUpOutVehicle();
        $parking = Parking::with('vehicle')->where('tag', 'A2')->first();
        $otherParking = Parking::with('vehicle')->where('tag', 'B2')->first();
        $data = [ 'key' => $parking->vehicle->license_plate];

        $this->actingAs($this->user)
            ->json('PUT', '/api/parking/out/vehicle', $data)
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'total_pay'
                ]
            ]);
        $this->assertSoftDeleted('parkings', [ 'ticket' => $parking->ticket]);
        $this->assertDatabaseMissing('parkings', [ 
            'ticket' => $otherParking->ticket,
            'tag' => $otherParking->tag
        ]);
        $this->assertDatabaseHas('parkings', [ 
            'ticket' => $otherParking->ticket,
        ]);
    }

    public function setUpParking()
    {
        foreach (['A','B'] as $row) {
            for ($i=1; $i <= 5; $i++) { 
                Parking::create([
                    'tag' => $row.$i
                ]);
            }
        }
    }

    public function setUpParkingCell($n = 1, $closed = false)
    {
        $faker = Faker::create();
        for ($i=0; $i < $n; $i++) {
            $all = Parking::getCellEmpty(); 
            if ($all->isNotEmpty()) {
                $cell = $all->random();
                $cell->update([
                    'ticket' => strtoupper(str_random(6)),
                    'vehicle_id' => $this->setUpVehicle($faker)->id,
                    'status' => true,
                    'set_at' => date('Y-m-d H:i:s')
                ]);
                
                if ($closed && $faker->randomElement([true, false])) {
                    $cell = Parking::find($cell->id);
                    $created = Carbon::parse($cell->set_at);
                    $deleted = $created->copy()->addSeconds($faker->numberBetween(1, 60));
                    $time = $created->diffInSeconds($deleted);
                    $total_time = Carbon::today()->addSeconds($time)->toTimeString();
                    $cell->update([
                        'status' => false,
                        'total_time' => $total_time,
                        'total_pay' => $time * 30,
                    ]);
                    Parking::create([
                        'tag' => $cell->tag
                    ]);
                    $cell->delete();
                }
            }
        }
    }

    public function setUpVehicle($faker)
    {
        return Vehicle::create([
            'brand' => $faker->randomElement([
                'Chevrolet', 'Fiat', 'Ford', 'Jeep', 'Toyota'
            ]), 
            'license_plate' => strtoupper(str_random(6))
        ]);
    }

    public function setUpOutVehicle()
    {
        $faker = Faker::create();
        Parking::where('tag', 'A2')->first()->update([
            'ticket' => strtoupper(str_random(6)),
            'vehicle_id' => $this->setUpVehicle($faker)->id,
            'status' => true,
            'set_at' => Carbon::now()->subSeconds(45)
        ]);
        Parking::where('tag', 'B2')->first()->update([
            'ticket' => strtoupper(str_random(6)),
            'vehicle_id' => $this->setUpVehicle($faker)->id,
            'status' => true,
            'set_at' => Carbon::now()->subSeconds(35)
        ]);
    }
}
