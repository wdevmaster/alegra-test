<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Tests\Browser\Pages\Login;
use Tests\Browser\Pages\Dashboard;
use Caffeinated\Shinobi\Models\Role;

class LoginTest extends DuskTestCase
{
    public function setUp()
    {
        parent::setup();

        static::closeAll();
    }

    /** @test */
    public function login_with_valid_credentials()
    {
        $user = factory(User::class)->create();
        $user->assignRole(
            factory(Role::class)->create(['special' => 'all-access'])->id
        );
        $page = new Dashboard();

        $this->browse(function ($browser) use ($page, $user) {
            $browser->visit(new Login)
                ->submit($user->email, 'secret')
                ->assertPathIs($page->url());
        });
    }

    /** @test */
    public function login_with_invalid_credentials()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                ->submit('test@test.app', 'secret')
                ->assertSee('These credentials do not match our records.');
        });
    }

    /** @test */
    public function log_out_the_user()
    {
        $user = factory(User::class)->create();
        $user->assignRole(
            factory(Role::class)->create(['special' => 'all-access'])->id
        );
        $page = new Login();

        $this->browse(function ($browser) use ($page, $user) {
            $browser->visit(new Login)
                ->submit($user->email, 'secret')
                ->on(new Dashboard)
                ->clickLogout()
                ->assertPathIs($page->url());
        });
    }
}
