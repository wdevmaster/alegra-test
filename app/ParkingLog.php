<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingLog extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parking_id', 'type', 'value',
    ];

    public function ticket()
    {
        return $this->belongsTo('App\Parking');
    }
}
