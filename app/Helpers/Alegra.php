<?php

namespace App\Helpers;

use GuzzleHttp\Psr7;
use GuzzleHttp\Client as GuzzleHttp; 
use GuzzleHttp\Exception\RequestException;

class Alegra 
{
    protected $http;

    protected $params;

    public function __construct() {
        $this->http = new GuzzleHttp([
            'base_uri' => 'https://app.alegra.com/api/v1/',
            
        ]);
        
        $this->params = [
            'Accept'        => 'application/json',
            'content-type'  => 'application/json',
            'auth'          => [ env('ALEGRA_USERNAME'), env('ALEGRA_TOKEN') ]
        ];
    }

    public function getInvoices()
    {
        try {
            return $this->setUpResponse(
                $this->http->request('GET', 'invoices', $this->params)
            );
        } catch (RequestException $e) {
            return $e->getMessage();
        }
    }

    public function postInvoices(Array $body)
    {
        try {
            return $this->setUpResponse(
                $this->http->request('POST', 'invoices', 
                    array_merge($this->params, ['body' => json_encode($body)])
                )
            );
        } catch (RequestException $e) {
            return $e->getMessage();
        }
    }

    public function postContacts(Array $body)
    {
        try {
            return $this->setUpResponse(
                $this->http->request('POST', 'contacts', 
                    array_merge($this->params, ['body' => json_encode($body)])
                )
            );
        } catch (RequestException $e) {
            return $e->getMessage();
        }
    }

    private function setUpResponse($response)
    {
        return [
            'data'      => json_decode($response->getBody()->getContents()),
            'status'    => $response->getStatusCode(),
            'statusText'=> $response->getReasonPhrase(),
            'headers'   => $response->getHeaders(),
        ];
    }

}
