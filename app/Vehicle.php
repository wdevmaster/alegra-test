<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand', 'license_plate',
    ];

    public function parkings()
    {
        return $this->hasMany('App\Parking');
    }
    
    public function setLicensePlateAttribute($value)
    {
        $this->attributes['license_plate'] = strtoupper($value); 
    }

    public function setBrandAttribute($value)
    {
        $this->attributes['brand'] = strtoupper($value); 
    }
}
