<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function getUserAuth(Request $request)
    {
        return new UserResource(
            User::with('roles')->find($request->user()->id)
        );
    }
}
