<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Vehicle;
use App\Parking;
use App\Helpers\Alegra;
use Illuminate\Support\Collection;
use App\Http\Resources\ParkingResource;

class ParkingController extends Controller
{
    public function getData(Request $request)
    {
        return [
            'data' => $this->setUpTable(Parking::getData())
        ];
    }

    public function getTickets(Request $request)
    {
        return [
            'data' => Parking::getTickects()
        ];
    }

    public function getBusiestCells(Request $request)
    {
        return [
            'data' => Parking::getBusiestCell()
        ]; 
    }

    public function setInVehicle(Request $request)
    {
        $this->validate($request, [
            'brand' => 'required',
            'license_plate' => 'required|min:4',
        ]);
        
        if ($vehicle = Vehicle::firstOrCreate([
            'brand' => $request->brand,
            'license_plate' => $request->license_plate
        ])) {
            $cells = Parking::getCellEmpty();
            $cell = $this->validateFirstCell($cells->random());
            $ticket = strtoupper(str_random(6));
            $cell->update([
                'ticket' => $ticket,
                'vehicle_id' => $vehicle->id,
                'status' => true,
                'set_at' => Carbon::now()->toDateTimeString()
            ]);
            $cell->logs()->create([
                'type' => 'IN',
                'value' => $cell->tag
            ]);
            $cell->logs()->create([
                'type' => 'OUT',
                'value' => $cell->tag
            ]);

            return [
                'data' => [
                    'ticket' => Parking::with('vehicle', 'logs')->find($cell->id)
                ]
            ];
        }
    }

    public function getDataVehicle(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
        ]);

        if ($parking = Parking::with('vehicle', 'logs')
            ->whereHas('vehicle', function ($query) use ($request) {
                $query->where('license_plate', $request->key);
            })->first()
        ){
            $time = Carbon::now()->diffInSeconds($parking->set_at);

            return [
                'data' => [
                    'id'        => $parking->id,
                    'tag'       => $parking->tag,
                    'vehicle'   => $parking->vehicle,
                    'logs'      => $parking->logs,
                    'status'    => $parking->status,
                    'time'      => $time,
                    'total'     => $time * 30,
                    'set_at'    => $parking->set_at,
                ]
            ];
        }
    }

    public function setOutVehicle(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
            'client' => 'required',
        ]);

        if ($parking = Parking::with('vehicle')->whereHas('vehicle', function ($query) use ($request) {
            $query->where('license_plate', $request->key);
        })->first()) {
            $pay = $this->outVehicle($parking);
            $this->alegraProcs($request, $parking);
            if ($postCell = $this->validatePosCell($parking)) {
                if (Parking::getCellEmpty()->count()) {
                    $where = array_merge($where,
                        $this->moveCellVehicle($postCell)
                    );
                }
            }
            
            return [ 'data' => [
                'total_pay' => $pay
            ]];
        }
    }
    
    public function alegraProcs($request, $parking)
    {
        if(!env('ALEGRA_DEBUG', true)) {
            $alegra = new Alegra();
            $seconds = Carbon::now()->diffInSeconds($parking->set_at);
            $res = $alegra->postContacts([
                'type' => 'client',
                'name' => $request->client,
            ]);
            $dataInvoice = [
                'date' => date('Y-m-d'),
                'dueDate' => date('Y-m-d'),
                'status' => 'open',
                'client' => $res['data']->id,
                'items' => [
                    [
                        'id' => 1,
                        'price' => 30,
                        'quantity' => $seconds,
                        'description' => 'Resguardo por: '.$seconds.'seg'
                    ]
                ]
            ];

            $alegra->postInvoices($dataInvoice);
        }
        
    }

    private function setUpTable($rows)
    {
        return  $rows->groupBy(function ($item) {
            return substr($item['tag'], 0, 1);
        })->map(function ($item){
            return $item->keyBy(function ($item){
                return substr($item['tag'], 1, 2);
            });
        });
    }

    private function validateFirstCell($cell)
    {
        if (substr($cell->tag, 0, 1) == 'B') {
            $tag = 'A'.substr($cell->tag, 1, 2);
            if ($newCell = Parking::where('tag', $tag)->where('status', 0)->first())
                return $newCell;
        }
        return $cell;
    }

    private function validatePosCell($parking)
    {
        foreach ($this->setUpTable(Parking::getData()) as $rowKey => $cells) {
            if ($rowKey != substr($parking->tag, 0, 1))
                if ($cells[substr($parking->tag, 1, 2)]->status)
                    return $cells[substr($parking->tag, 1, 2)];
        }
    }

    private function outVehicle($parking)
    {
        $seconds = Carbon::now()->diffInSeconds($parking->set_at);
        $parking->update([
            'status' => false,
            'total_pay' => $seconds * 30, 
            'total_time' => $seconds,
        ]);
        Parking::create([
            'tag' => $parking->tag
        ]);
        $parking->delete();
        return $seconds * 30;
    }

    private function moveCellVehicle($postCell)
    {
        $cells = Parking::getCellEmpty();
        $cell = $this->validateFirstCell($cells->random());
        if ($cell->update([
            'ticket' => $postCell->ticket,
            'vehicle_id' => $postCell->vehicle->id,
            'status' => true,
            'set_at' => $postCell->set_at
        ])) {
            $cell->logs()->create([
                'type' => 'IN',
                'value' => $postCell->tag
            ]);
            $cell->logs()->create([
                'type' => 'OUT',
                'value' => $cell->tag
            ]);
            $postCell->update([
                'ticket' => NULL,
                'vehicle_id' => NULL,
                'status' => false,
                'set_at' => NULL,
            ]);
            $postCell->logs()->delete();
        }

        return [ $cell->tag, $postCell->tag ];
    }
}
