<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'role'  => [
                'id'        => $this->roles->first()->id,
                'name'      => $this->roles->first()->name,
                'slug'      => $this->roles->first()->slug,
                'special'   => $this->roles->first()->special
            ]
        ];
    }
}
