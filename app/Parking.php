<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parking extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag', 'ticket', 'vehicle_id', 'total_pay', 'total_time', 'status', 'set_at'
    ];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function logs()
    {
        return $this->hasMany('App\ParkingLog');
    }

    public function getTicketAttribute($value)
    {
        return strtoupper($value); 
    }

    public static function getData()
    {
        return self::orderBy('tag', 'asc')->get();
    }

    public static function getTickects()
    {
        return self::with('vehicle', 'logs')->where('status', 1)
            ->orderBy('set_at', 'desc')->get();
    }

    public static function getCellEmpty()
    {
        return self::where('status', 0)
            ->orderBy('set_at', 'desc')->get(); 
    }

    public static function getBusiestCell()
    {
        return self::selectRaw('tag, sum(total_time) as time, sum(total_pay) as total')
            ->onlyTrashed()->groupBy('tag')->orderBy('time', 'desc')->offset(0)->limit(5)
            ->get();
    }
}
