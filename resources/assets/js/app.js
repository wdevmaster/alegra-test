import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import { i18n } from '~/plugins'
import Vuelidate from 'vuelidate'
import FormatJs from 'vue-filter-number-format'
import App from '~/components/App'

import '~/components'

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.filter('FormatJs', FormatJs)

new Vue({
  i18n,
  store,
  router,
  ...App
})
