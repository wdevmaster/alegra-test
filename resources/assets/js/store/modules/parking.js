import axios from 'axios'
import * as types from '../mutation-types'

//state
export const state = {
  parking: [],
  tickets: [],
  busiestCells: []
}

// mutations
export const mutations = {
  [types.DATA_PARKING_TABLE_SUCCESS] (state, { parking }) {
    state.parking = parking
  },

  [types.DATA_PARKING_TABLE_FAILURE] (state) {
    state.parking = null
  },

  [types.UPDATE_PARKING_TABLE] (state, { table }) {
    let rowIndex = table.tag.substr(0, 1)
    let cellIndex = table.tag.substr(1)
    state.parking[rowIndex][cellIndex] = table
  },

  [types.LIST_TICKETS_SUCCESS] (state, { tickets }) {
    state.tickets = tickets
  },

  [types.LIST_TICKETS_FAILURE] (state) {
    state.tickets = null
  },

  [types.UPDATE_TICKET] (state, { ticket }) {
    state.tickets.unshift(ticket)
  },

  [types.BUSIEST_CELLS_SUCCESS] (state, { busiestCells }) {
    state.busiestCells = busiestCells
  },

  [types.BUSIEST_CELLS_FAILURE] (state) {
    state.busiestCells = null
  },
}

// actions
export const actions = {
  async getData ({ commit }) {
    try {
      const { data } = await axios.get('/api/parking/data')
      commit(types.DATA_PARKING_TABLE_SUCCESS, { parking: data.data })
    } catch (e) {
      commit(types.DATA_PARKING_TABLE_FAILURE)
    }
  },

  updateTable ({ commit, dispatch }, payload) {
    commit(types.UPDATE_PARKING_TABLE, payload)
  },

  async getTickets ({ commit }) {
    try {
      const { data } = await axios.get('/api/parking/tickets')
      commit(types.LIST_TICKETS_SUCCESS, { tickets: data.data })
    } catch (e) {
      commit(types.LIST_TICKETS_FAILURE)
    }
  },

  updateTickets ({ commit, dispatch }, payload) {
    commit(types.UPDATE_TICKET, payload)
  },

  async getBusiestCells ({ commit }) {
    try {
      const { data } = await axios.get('/api/parking/busiest')
      commit(types.BUSIEST_CELLS_SUCCESS, { busiestCells: data.data })
    } catch (e) {
      commit(types.BUSIEST_CELLS_FAILURE)
    }
  }
}

// getters
export const getters = {
  tableParking: state => state.parking,
  tickets: state => state.tickets,
  busiestCells: state => state.busiestCells
}