export default ({ authGuard, guestGuard }) => [
  { path: '/', name: 'welcome', component: require('~pages/auth/login.vue') },

  // Authenticated routes.
  ...authGuard([
    { path: '/dashboard', name: 'dashboard', component: require('~pages/dashboard.vue') },
  ]),

  // Guest routes.
  ...guestGuard([
    { path: '/login', name: 'login', component: require('~pages/auth/login.vue') },
  ]),

  { path: '*', component: require('~/pages/errors/404.vue') }
]
