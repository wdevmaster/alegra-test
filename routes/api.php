<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Helpers\Alegra;

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/auth', 'UserController@getUserAuth');

    Route::group(['prefix' => 'parking'], function() {
        Route::get('/data', 'ParkingController@getData');
        Route::get('/busiest', 'ParkingController@getBusiestCells');
        Route::get('/tickets', 'ParkingController@getTickets');
        Route::get('/vehicle', 'ParkingController@getDataVehicle');
        Route::post('/in/vehicle', 'ParkingController@setInVehicle');
        Route::put('/out/vehicle', 'ParkingController@setOutVehicle');
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
});