<?php

use Faker\Generator as Faker;
use Caffeinated\Shinobi\Models\Role;

$factory->define(Role::class, function (Faker $faker) {
    $name = $faker->jobTitle;
    return [
        'name' => $name, 
        'slug' => str_slug($name), 
        'special' => $faker->randomElement(['all-access', 'no-access'])
    ];
});
