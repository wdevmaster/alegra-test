<?php

use Illuminate\Database\Seeder;

use App\Parking;
use Carbon\Carbon;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ParkingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        /*
        $faker = Faker::create();
        for ($i=0; $i < 100; $i++) {
            $all = Parking::getCellEmpty(); 
            if ($all->isNotEmpty()) {
                $cell = $all->random();
                if ($closed = $faker->randomElement([true, false])) 
                    $cell->update([
                        'ticket' => str_random(6),
                        'vehicle_id' => 1,
                        'status' => true,
                        'set_at' => date('Y-m-d H:i:s')
                    ]);
                
                if ($closed && $faker->randomElement([true, false])) {
                    $cell = Parking::find($cell->id);
                    $created = Carbon::parse($cell->set_at);
                    $deleted = $created->copy()->addSeconds($faker->numberBetween(1, 60));
                    $time = $created->diffInSeconds($deleted);
                    $total_time = Carbon::today()->addSeconds($time)->toTimeString();
                    $cell->update([
                        'status' => false,
                        'total_time' => $total_time,
                        'total_pay' => $time * 30,
                    ]);
                    Parking::create([
                        'tag' => $cell->tag
                    ]);
                    $cell->delete();
                }
            }
        }
        */
    }
}
