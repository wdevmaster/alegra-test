<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::first();

        $admin = factory(App\User::class)->create([
            'name'  => 'Francisco', 
            'email' => 'francisco@email.com',
        ]);

        $admin->assignRole($roleAdmin->id);
    }
}
