<?php

use Illuminate\Database\Seeder;
use App\Parking;

class ParkingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['A','B'] as $row) {
            for ($i=1; $i <= 10; $i++) { 
                Parking::create([
                    'tag' => $row.$i
                ]);
            }
        }
    }
}
